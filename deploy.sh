#!/bin/bash

yarn build
rsync -e "ssh -p 15022" -avz --delete dist/spa/ liederbuch@endivie.livingutopia.org:/home/liederbuch/html
