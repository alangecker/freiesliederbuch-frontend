
const routes = [
  { path: '/editor', name:'editor', component: () => import('pages/Editor.vue') },
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '/song/:hash/:slug', name:'song', component: () => import('pages/Song.vue') },
      { path: '', component: () => import('pages/Song.vue') }
    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
